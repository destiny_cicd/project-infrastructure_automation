provider "aws" {
  access_key = "AKIAUZXKVNYMT32QRZWI"
  secret_key  = "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"
  region          = "us-east-1"
}

resource "aws_key_pair" "push_auth_key" {
  key_name   = "authentic-keyz"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZjbmPLRYwxCT3Y70npFU08nietoQcC/kZ8PqaGioC2WI6DEWF39f6CQOTQEgz0Al+qWge5JFz+RG7pOsOQXC/2qTQeNwq8OfGzW4FCzxTFbqI+PTrs7+f174ILbd6oZYgRGrgYObr5rFSrA/7owKXJpOiRDnKUSWy7nrO6IDWdC88froec8TT+pnfTGbxGKqWKZWm7mXYKLnymz2ovJvJ6BtckMcBq5HGQt9uIGe6tL6chBCWmoVOvN1W/Sku/WlEYN9iEITpLik9iZYN2dcAZo07jYSyUMMtCztxEamVUoLa6CoxU9mbwXHcCBhW+61L+1reWvmKxNQhNYIWSuZL spark-user@baseImage"
  }

resource "aws_security_group" "VPC_Gateway" {
  ingress {
        from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

  egress {
        from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "Atlantis" {
  instance_type = "${var.instance_type}"
  ami = "ami-03597b1b84c02cf7b"
  count = "${var.instance_count}"  
  key_name = "authentic-keyz"
  vpc_security_group_ids = ["${aws_security_group.VPC_Gateway.id}"]
  associate_public_ip_address = true
}

resource "aws_elb" "load_balancer" {

 name = "Atlas-Elbz"
 availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1e"]
 instances = ["${aws_instance.Atlantis.*.id}"]
 cross_zone_load_balancing = true
 idle_timeout = 400
 connection_draining = true
 connection_draining_timeout = 400
 
  listener {
    instance_port     = 8081
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  
    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8081/"
    interval            = 30
  }
  
}
resource "ansible_host" "default" {
  count = "${aws_instance.Atlantis.count}"
  inventory_hostname = "${aws_instance.Atlantis.*.id[count.index]}"
  vars {
    ansible_user = "ubuntu"
    ansible_host = "${aws_instance.Atlantis.*.public_ip[count.index]}"
  }
}
